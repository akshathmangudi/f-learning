def main():
    print("Introduction to federated learning...")
    trainloader, testloader, _ = load()
    print("Start training")
    model = BaseModel().to(device)
    train(model=model, trainloader=trainloader, epochs=10, device=device)
    print("Evaluating model")
    loss, accuracy = test(model=model, testloader=testloader, device=device)
    print("Loss: ", loss)
    print("Accuracy: ", accuracy)

if __name__ == "__main__":
    main()

